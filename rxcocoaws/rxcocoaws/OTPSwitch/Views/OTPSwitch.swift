//
//  OTPSwitch.swift
//  OTPUI
//
//  Created by David Horvath Hori on 2017. 10. 06..
//  Copyright © 2017. Supercharge Ltd. All rights reserved.
//

import UIKit

@IBDesignable
public class OTPSwitch: OTPCustomView {
    // MARK: - Variables
    public var materialSwitch: OTPMaterialSwitch!
    @IBInspectable private var thumbOnTintColor: UIColor! = UIColor.white
    @IBInspectable private var thumbOffTintColor: UIColor! = UIColor.white
    @IBInspectable private var trackOnTintColor: UIColor! = UIColor.red
    @IBInspectable private var trackOffTintColor: UIColor! = UIColor.gray
    @IBInspectable private var thumbDisabledTintColor: UIColor! = UIColor.gray
    @IBInspectable private var trackDisabledTintColor: UIColor! = UIColor.gray
    @IBInspectable private var state: Bool = false

    public var isOn: Bool {
        set {
            materialSwitch.setOn(on: newValue, animated: true)
        }
        get {
            return materialSwitch.isOn
        }
    }

    public var selectionChanged: ((_ isSelected: Bool) -> Void)?

    // MARK: - Setup
    override public func setup() {
        materialSwitch = OTPMaterialSwitch(withSize: .normal,
                                           thumbOnTintColor: thumbOnTintColor,
                                           thumbOffTintColor: thumbOffTintColor,
                                           trackOnTintColor: trackOnTintColor,
                                           trackOffTintColor: trackOffTintColor,
                                           thumbDisabledTintColor: thumbDisabledTintColor,
                                           trackDisabledTintColor: trackDisabledTintColor)
        materialSwitch.delegate = self
        materialSwitch.accessibilityIdentifier = accessibilityIdentifier
        contentView?.addSubview(materialSwitch)
        materialSwitch?.anchorToSuperview()
    }

    public func set(state: Bool?) {
        guard let state = state else {
            materialSwitch.setInitial(state: self.state)
            return
        }
        self.state = state
        materialSwitch.setInitial(state: state)
    }
}

extension OTPSwitch: OTPMaterialSwitchDelegate {
    public func switchStateChanged(_ currentState: OTPMaterialSwitchState) {
        self.isOn = currentState == .on
        selectionChanged?(currentState == .on)
    }

    public func valueChanged(_ enabled: Bool) {
        // no-op
    }
}

public protocol CustomViewable {
    var contentView: UIView? { get set }
    func setup()
}

public class OTPCustomView: OTPView, CustomViewable {
    public var contentView: UIView?

    public func setup() {
        // no-op
    }

    // MARK: - Lifecycle
    override public func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }

    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
    }

    internal func xibSetup() {
        guard let view = loadFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        setup()
    }
}

open class OTPView: UIView {
    @IBInspectable private var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable private var borderColor: UIColor? {
        set { layer.borderColor = newValue!.cgColor }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }

    public func set(cornerRadius: CGFloat) {
        self.cornerRadius = cornerRadius
    }
}

extension UIView {
    public func loadFromNib() -> UIView? {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }

    public func anchorToSuperview(top: Bool = true, bottom: Bool = true, leading: Bool = true, trailing: Bool = true) {
        guard let superview = self.superview else {
            return
        }
        self.topAnchor.constraint(equalTo: superview.topAnchor).isActive = top
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = bottom
        self.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = leading
        self.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = trailing
    }
}
