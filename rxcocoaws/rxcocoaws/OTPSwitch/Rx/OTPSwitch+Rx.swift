//
//  OTPSwitch+Rx.swift
//  rxcocoaws
//
//  Created by Nagy Ádám on 2018. 11. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa

// MARK: - Binders
extension Reactive where Base: OTPSwitch {
    var isBounceEnabled: Binder<Bool> {
        return Binder(self.base.materialSwitch) { view, isBounceEnabled in
            view.isBounceEnabled = isBounceEnabled
        }
    }
}

// MARK: - Control Properties
extension Reactive where Base: OTPSwitch {
    var isOn: ControlProperty<Bool> {
        return base.materialSwitch.rx.controlProperty(
            editingEvents: UIControlEvents.allTouchEvents,
            getter: { (materialSwitch) in
                return materialSwitch.getSwitchState()
        },
            setter: { (materialSwitch, boolean) in
                if boolean == true {
                    materialSwitch.changeThumbStateONwithAnimation()
                } else {
                    materialSwitch.changeThumbStateOFFwithAnimation()
                }
        })
    }

    var valueChanged: Observable<Bool> {
        return base.materialSwitch.rx.value
    }

    var stateChanged: Observable<OTPMaterialSwitchState> {
        return base.materialSwitch.rx.state
    }
}
