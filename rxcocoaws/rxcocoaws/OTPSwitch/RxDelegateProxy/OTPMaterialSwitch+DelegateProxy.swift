//
//  OTPMaterialSwitch+DelegateProxy.swift
//  rxcocoaws
//
//  Created by Nagy Ádám on 2018. 11. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa

class RxOTPSwitchDelegateProxy
: DelegateProxy<OTPMaterialSwitch, OTPMaterialSwitchDelegate>, DelegateProxyType, OTPMaterialSwitchDelegate {
    public weak private(set) var materialSwitch: OTPMaterialSwitch?

    let state = PublishSubject<OTPMaterialSwitchState>()
    let value = PublishSubject<Bool>()

    public init(materialSwitch: OTPMaterialSwitch) {
        self.materialSwitch = materialSwitch
        super.init(parentObject: materialSwitch, delegateProxy: RxOTPSwitchDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        self.register { RxOTPSwitchDelegateProxy(materialSwitch: $0) }
    }

    static func currentDelegate(for object: OTPMaterialSwitch) -> OTPMaterialSwitchDelegate? {
        return object.delegate
    }

    static func setCurrentDelegate(_ delegate: OTPMaterialSwitchDelegate?, to object: OTPMaterialSwitch) {
        object.delegate = delegate
    }

    // Delegates
    func switchStateChanged(_ currentState: OTPMaterialSwitchState) {
        state.onNext(currentState)
    }

    func valueChanged(_ enabled: Bool) {
        value.onNext(enabled)
    }
}
