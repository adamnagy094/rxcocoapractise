//
//  ViewController.swift
//  rxcocoaws
//
//  Created by Edem on 2018. 11. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa

typealias ColorStyleType = (background: UIColor, textfield: UIColor)

enum ColorStyle {
    static let basic: ColorStyleType = (UIColor.white, UIColor.gray)
    static let dark: ColorStyleType = (UIColor.black, UIColor.red)
}

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var otpSwitch: OTPSwitch!

    var design = BehaviorRelay<ColorStyleType>(value: ColorStyle.basic)

    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBinding()
    }

    private func setupBinding() {
        let designDriver = design.asDriver()

        designDriver.map({ $0.background })
            .drive(view.rx.backgroundColor)
            .disposed(by: bag)

        designDriver.map({ $0.textfield })
            .drive(textfield.rx.backgroundColor)
            .disposed(by: bag)

        otpSwitch.rx.valueChanged
            .map { $0 ? ColorStyle.dark : ColorStyle.basic }
            .asDriver(onErrorJustReturn: ColorStyle.basic)
            .drive(design)
            .disposed(by: bag)

        otpSwitch.rx.stateChanged
            .subscribe(onNext: { print("Switch new state: \($0)") })
            .disposed(by: bag)

        textfield.rx.text
            .map { $0?.uppercased() }
            .asDriver(onErrorJustReturn: "")
            .drive(label.rx.text)
            .disposed(by: bag)

        textfield.rx.text
            .map { ($0 ?? "").count < 1 }
            .asDriver(onErrorJustReturn: false)
            .drive(label.rx.isHidden)
            .disposed(by: bag)

        textfield.rx.text
            .map { ($0 ?? "").count }
            .map { ($0 % 2 == 0) ? UIColor.black : UIColor.white }
            .bind(to: label.rx.color)
            .disposed(by: bag)
    }
}

extension Reactive where Base: UILabel {
    var color: Binder<UIColor> {
        return Binder(self.base) { label, color in
            label.textColor = color
        }
    }
}

extension Reactive where Base: UITextField {
    var backgroundColor: Binder<UIColor> {
        return Binder(self.base) { field, color in
            field.backgroundColor = color
        }
    }
}

extension Reactive where Base: UIView {
    var backgroundColor: Binder<UIColor> {
        return Binder(self.base) { view, color in
            view.backgroundColor = color
        }
    }
}
